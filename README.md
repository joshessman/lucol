# LUCOL
## A stochastically verified compiler for aerospace control systems

## FAQ

* **What is LUCOL?**
  
  See [An Approach to Software for High Integrity Applications](https://asmedigitalcollection.asme.org/GT/proceedings/GT1982/79603/V005T14A006/234914). 

* **What does this project provide?**
  
  This tool is a LLVM front end for LUCOL (LUcas COntrol Lanuage), that is, 
  it facilitates the translation of LUCOL into LLVM IR which can subsequently
  be compiled to native machine code and linked just like any other object code. 

* **What is LLVM?**

  LLVM, formerly Low-Level Virtual Machine, is an open-source compiler infrastructure.
  Its C/C++ front end, Clang, is the leading competitor to GCC in the field of open-source compilers.
  
* **What is a compiler front end?**

  Compilation is frequently divided into three phases: the front end, the middle end, and the back end.
  The front end handles the lexing (tokenizing), parsing, and construction of an AST (abstract syntax tree).
  The idea is to create a "distilled" version of the source code that can be easily optimized by the middle end
  and easily translated to the machine code understood by the target processor.  Generally, the output of a 
  compiler front end is some form of IR (intermediate representation).  This is a low-level assembly language
  that is source-language agnostic and provides the abstraction layer that allows existing compilers
  to support new languages without changing the middle or back ends - only a new front end is required.

* **Why LLVM?**

  LLVM was chosen over GCC (the only other major open-source compiler infrastructure) as the back end because its IR is much more
  well-defined and robust - the GCC IR (GIMPLE), for example, is not type safe.  The LLVM IR is 
  statically typed.  The GCC project tends to focus more on making new back ends easier to implement
  and as a result LLVM has become the de facto standard for new language front ends.
  
## Requirements
* LLVM: 
  - On Debian/Ubuntu: `apt-get install llvm-7 llvm-7-dev`
* libedit:
  - On Debian/Ubuntu: `apt-get install libedit-dev`

## How to Build
This compiler uses CMake:
```sh
mkdir build
cd build
cmake ..
make
make install
```

The compiler will be, by default, installed in `~/.local/bin`.

## How to Use
1. Use the Python script, convert_prototypes.py, to generate the prototype file.  
The script is currently in `utils`, but may be moved.  The first and only
command-line argument to the script should be the name of the C header file 
containing the function signatures for the modules.

2. Call the compiler itself
Navigate to the compiler's location (if not installed in your `$PATH`) and 
run the following:

```sh
LUCOL -p /path/to/prototype/file /path/to/src.luc
```
  
The list of command-line options is as follows:
* `-p`: Used to denote the prototype file containing the function signatures for the LUCOL modules
* `-w`: Used to select "weak mode", where global definitions have weak linkage.  This is intended for cases where the global definitions are placeholders intended to be overwritten at link time.