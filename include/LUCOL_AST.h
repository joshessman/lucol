// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#ifndef LUCOL_AST_H
#define LUCOL_AST_H

/// @brief Definition of LUCOL AST nodes, each of which represents a
/// syntactical feature of the language, e.g. case statement, variable declaration, etc

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <utility>

#include "LUCOL.h"
#include "LUCOL_Lexer.h"

#include <llvm/IR/IRBuilder.h>

using namespace llvm;

/// @brief Source code info struct for individual AST nodes
struct LUCOL_Source_Info
{
  unsigned int line;
  unsigned int col;
};

/// @brief Base class for AST nodes
class LUCOL_AST
{
private:
  LUCOL_Source_Info loc;

public:
  /// @brief Constructs base class by initializing source info from current pos
  LUCOL_AST()
  {
    loc = {LUCOL_File_Position::line_num, LUCOL_File_Position::col_num};
  }
  virtual ~LUCOL_AST() {}
  /// @brief Pure virtual function to ensure that all derived classes
  /// provide a way to generate IR such that the tree can be traversed and
  /// generated
  virtual Value *generate_ir() const = 0;
  LUCOL_Source_Info get_loc() const { return loc; }
};

/// @brief AST node for integer literals
class LUCOL_AST_Literal : public LUCOL_AST
{
private:
  short val;

public:
  LUCOL_AST_Literal(const short v) : val(v) {}

  /// @brief Generation of LLVM IR for integer literal expressions
  Value *generate_ir() const override;
};

/// @brief AST node for using a variable
class LUCOL_AST_Variable : public LUCOL_AST
{
private:
  string name;
  bool is_ptr;

public:
  LUCOL_AST_Variable(const string &n, bool is_p) : name(n), is_ptr(is_p) {}
  /// @brief Generation of LLVM IR for reference of variable
  Value *generate_ir() const override;
};

/// @brief AST node for variadic function call terminators
class LUCOL_AST_Terminator : public LUCOL_AST
{
public:
  LUCOL_AST_Terminator() {}

  /// @brief Generation of LLVM IR for variadic terminator expressions
  Value *generate_ir() const override;
};

/// @brief AST node for module and segment/procedure calls
class LUCOL_AST_Call : public LUCOL_AST
{
private:
  string name;
  std::vector<std::unique_ptr<LUCOL_AST>> args;

public:
  LUCOL_AST_Call(const string &n, std::vector<std::unique_ptr<LUCOL_AST>> &&a = {}) : name(n), args(std::move(a)) {}

  /// @brief Generation of LLVM IR for segment/procedure and module calls
  Value *generate_ir() const override;
};

/// @brief AST node for module prototypes
class LUCOL_AST_Prototype : public LUCOL_AST
{
private:
  string name;
  std::vector<string> args;
  bool is_variadic;

public:
  LUCOL_AST_Prototype(const string &n, std::vector<string> a = {}, bool variadic = false) : name(n), args(std::move(a)), is_variadic(variadic) {}
  string get_name() { return name; }

  /// @brief Generation of LLVM IR for module and segment/procedure prototypes
  Function *generate_ir() const override;
};

/// @brief AST node for segment/procedure definitions
class LUCOL_AST_Definition : public LUCOL_AST
{
private:
  std::unique_ptr<LUCOL_AST_Prototype> proto;
  std::vector<std::unique_ptr<LUCOL_AST>> statements;

public:
  LUCOL_AST_Definition(std::unique_ptr<LUCOL_AST_Prototype> &&p, std::vector<std::unique_ptr<LUCOL_AST>> &&s) : proto(std::move(p)), statements(std::move(s)) {}

  /// @brief Generation of LLVM IR for segment/procedure definitions
  Function *generate_ir() const override;
};

/// @brief AST node for variable declarations
class LUCOL_AST_Declaration : public LUCOL_AST
{
private:
  std::pair<string, std::vector<std::unique_ptr<LUCOL_AST>>> initializer;
  LUCOL_Type type;
  bool is_alias;
  unsigned int array_size;
  bool force_global;

public:
  LUCOL_AST_Declaration(std::pair<string, std::vector<std::unique_ptr<LUCOL_AST>>> &&init,
                        const LUCOL_Type t, const bool alias = false, const int ar_sz = 0, const bool ucd_global = false) : initializer(std::move(init)), type(t), is_alias(alias), array_size(ar_sz), force_global(ucd_global) {}

  /// @brief Generation of LLVM IR for LUCOL variable declarations/initializations
  Value *generate_ir() const override;
};

/// @brief AST node for if-then-else pattern
class LUCOL_AST_Conditional : public LUCOL_AST
{
private:
  std::unique_ptr<LUCOL_AST> condition;
  std::vector<std::unique_ptr<LUCOL_AST>> if_block;
  std::vector<std::unique_ptr<LUCOL_AST>> else_block;

public:
  LUCOL_AST_Conditional(std::unique_ptr<LUCOL_AST> &&cond,
                        std::vector<std::unique_ptr<LUCOL_AST>> &&i,
                        std::vector<std::unique_ptr<LUCOL_AST>> &&e) : condition(std::move(cond)), if_block(std::move(i)), else_block(std::move(e)) {}

  /// @brief Generation of LLVM IR for if-then-else statements
  Value *generate_ir() const override;
};

/// @brief AST node for switch-case statement
class LUCOL_AST_Case : public LUCOL_AST
{
private:
  std::unique_ptr<LUCOL_AST> condition;
  std::unordered_map<string, std::vector<std::unique_ptr<LUCOL_AST>>> case_blocks;
  std::vector<std::unique_ptr<LUCOL_AST>> default_block;
  std::unordered_map<string, std::vector<unsigned int>> case_map;

public:
  LUCOL_AST_Case(std::unique_ptr<LUCOL_AST> &&cond,
                 std::unordered_map<string, std::vector<std::unique_ptr<LUCOL_AST>>> &&cases,
                 std::vector<std::unique_ptr<LUCOL_AST>> &&d_block,
                 std::unordered_map<string, std::vector<unsigned int>> &&mappings) : condition(std::move(cond)), case_blocks(std::move(cases)), default_block(std::move(d_block)), case_map(std::move(mappings)) {}

  /// @brief Generation of LLVM IR for case statements
  Value *generate_ir() const override;
};

#endif
