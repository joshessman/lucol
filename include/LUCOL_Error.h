// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#ifndef LUCOL_ERROR_H
#define LUCOL_ERROR_H

/// @brief Definition of LUCOL exception classes to report errors during the
/// parsing and IR generation stages

#include <stdexcept>
#include <string>

using std::string;

#include "LUCOL_Lexer.h" // For file position information

/// @brief Class inheriting from std::exception to handle
/// issues encounted during the parse stage
class LUCOL_Parse_Error : public std::runtime_error
{
public:
  LUCOL_Parse_Error(const string msg_str) : std::runtime_error("Parse error at " +
                                                               LUCOL_File_Position::file_name + ":" +
                                                               std::to_string(LUCOL_File_Position::line_num) + ":" +
                                                               std::to_string(LUCOL_File_Position::col_num) + ": " + msg_str) {}
};

/// @brief Class inheriting from std::exception to handle
/// issues encounted during the LLVM IR generation stage
class LUCOL_IR_Error : public std::runtime_error
{
public:
  // The "read head" will likely have passed the code location where the erroneous call
  // occurs, in order to provide more detailed info the file position paradigm will need to
  // change s.t. each AST node has its own file position member
  LUCOL_IR_Error(const string msg_str) : std::runtime_error("IR generation error for block ending at " +
                                                            LUCOL_File_Position::file_name + ":" +
                                                            std::to_string(LUCOL_File_Position::line_num) + ":" +
                                                            std::to_string(LUCOL_File_Position::col_num) + ": " + msg_str) {}
};

#endif