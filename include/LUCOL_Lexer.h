// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#ifndef LUCOL_LEXER_H
#define LUCOL_LEXER_H

/// @brief Class definitions for Token type and the LUCOL lexer (tokenizer)

#include <string>
#include <iostream>
#include <stack>
#include <regex>

using std::string;

/// @brief An enumeration type for top-level tokens
enum class Token
{
  // File related
  eof,
  newline,

  // Control flow
  if_cond,
  else_cond,
  endif,
  case_init,
  case_entry,
  case_default,
  case_end,

  // Interrupt level control
  level,
  pwrup,
  exec,

  // Commands
  subroutine_start,
  subroutine_end,
  variable_def,
  subroutine_call,

  // Single-character tokens
  lparen,
  rparen,
  comma,
  lbracket,
  rbracket,
  equals,
  colon,

  // Primary token types
  identifier,
  lucif, // LUCOL implicit register/accumulator
  literal,

  // Error conditions
  error
};

/// @brief A singleton for storing global file position
struct LUCOL_File_Position
{
  static string file_name;
  static unsigned int line_num;
  static unsigned int col_num;
};

// WHY WOULD THE GNU PROJECT DO THIS???????
// https://stackoverflow.com/questions/12530406/is-gcc-4-8-or-earlier-buggy-about-regular-expressions
namespace no_regex
{
  bool match_ident(string token);
  bool match_int(string token);
  bool match_hex(string token);
} // namespace no_regex

/// @brief A lexer for the LUCOL language that tokenizes input and identifies
/// the tokens
class LUCOL_Lexer
{
private:
  string ident;               // temporary storage for identifier tokens and keywords
  short literal_val;          // temporary storage for 16-bit integer literals
  std::istream &input_stream; // The stream to read LUCOL off of
  Token cur_tok;
  char curr_char; // The character most recently read in from the file
  // Pending upgrade from GCC 4.8.5
  // const std::regex ident_pattern;
  // const std::regex int_pattern;
  // const std::regex hex_pattern;

  /// @brief Gets the next character off the input stream and increments the
  /// read head position
  char get_next_char() const;

  /// @brief Ignores the rest of the line on the input (for comments etc)
  /// @post The current line is ignored and the first token on the next
  /// non-comment line is returned
  /// @return The next token in the stream after comments are ignored
  Token ignore_line();

  /// @brief The lexing function that grabs the next token from the file input
  /// @post The values of the internal variables storing the read-in identifiers
  /// and numeric literals may be updated
  /// @return The type of the token read in
  Token grab();

public:
  // @brief Constructor to initialize desired input stream
  LUCOL_Lexer(std::istream &input) : input_stream(input), curr_char(' ') {}

  /// @brief Wrapper for grab() allowing 1-token lookahead for the parser
  Token operator()();

  /// @brief Accessor for stored integer literals
  /// @pre cur_tok must be Token::literal
  /// @return The literal value of the current token (16-bit signed) is returned
  short get_literal() const
  {
    if (cur_tok != Token::literal)
    {
      throw std::runtime_error("Lexer literal cannot be accessed unless current token is literal");
    }
    return literal_val;
  }

  /// @brief Accessor for stored identifiers/keywords
  /// @pre cur_tok must be Token::identifier
  /// @return The string corresponding to the current token
  string get_ident() const
  {
    // TODO: Should we be grabbing the identifier string to check the variable type
    // or should each type have its own token?
    if ((cur_tok != Token::identifier) && (cur_tok != Token::variable_def))
    {
      throw std::runtime_error("Lexer identifier cannot be accessed unless current token is identifier");
    }
    return ident;
  }

  /// @brief Accessor for current token
  Token get_cur_tok() const { return cur_tok; }
};

#endif