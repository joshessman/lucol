// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#ifndef LUCOL_IR_H
#define LUCOL_IR_H

/// @brief Includes for LLVM IR/compilation in addition to singleton class for
/// storing IR generation helpers

#include <string>
#include <memory>
#include <utility>

using std::string;

// LLVM includes - tree may differ on other platforms
#include <llvm/ADT/APInt.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#ifdef LLVM3
#include <llvm/Analysis/Verifier.h>
#else
#include <llvm/IR/Verifier.h>
#endif

using namespace llvm;

/// @brief Symbol table entry class to encapsulate all necessary information for
/// IR generation during AST visis
struct LUCOL_Symbol
{
  Value *val;
  bool is_alias;
};

/// @brief Singleton class for actual IR generation
struct LUCOL_IR_Generator
{
  static LLVMContext context;
  static IRBuilder<> ir_builder;
  static std::unique_ptr<Module> module;
  static std::map<std::string, LUCOL_Symbol> symbol_table;
  static std::stack<std::string> function_stack;
  static std::map<std::string, std::map<std::string, LUCOL_Symbol>> local_symbols;
  static bool is_define_file;
};
#endif