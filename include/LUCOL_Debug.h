// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#ifndef LUCOL_DEBUG_H
#define LUCOL_DEBUG_H

#include <memory>
#include <vector>
#include "llvm/IR/DIBuilder.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "LUCOL_AST.h"

using namespace llvm;

/// @brief Singleton class for DWARF helper
struct DWARF_Info
{
  static DICompileUnit *compile_unit;
  static DIType *i8_type;
  static DIType *i8_ptr_type;
  static DIType *i16_type;
  static DIType *i16_ptr_type;
  // static DIType* i16_arr_type;
  static DIType *get_i8_type();
  static DIType *get_i8_ptr_type();
  static DIType *get_i16_type();
  static DIType *get_i16_arr_type();
  static DIType *get_i16_ptr_type();
  static void emit_location(LUCOL_Source_Info loc);
  static std::vector<DIScope *> scope_blocks;
};

/// @brief Singleton class for DWARF builder
struct LUCOL_Debug
{
  static std::unique_ptr<DIBuilder> dbg_builder;
};

#endif