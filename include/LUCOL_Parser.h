// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#ifndef LUCOL_PARSER_H
#define LUCOL_PARSER_H

/// @brief Class definitions for the LUCOL parser that generates the AST
/// for a given program

#include <memory>
#include <utility>
#include "LUCOL_Lexer.h"
#include "LUCOL_AST.h"

#include <iostream>

using std::cout;
using std::endl;

/// @brief A parser to construct an AST given LUCOL source
class LUCOL_Parser
{
private:
  LUCOL_Lexer lex;

public:
  /// @brief Constructor to pass-through an input stream to the lexer
  /// @param input The istream to read the LUCOL from
  LUCOL_Parser(std::istream &input = std::cin) : lex(input) {}

  /// @brief Primes the lexer with the first token off the input stream
  void initialize() { lex(); }

  /// @brief Constructs an AST node for a LUCOL external declaration
  /// @note: <external-declaration> ::= <subroutine-definition>
  ///                                 | <variable-declaration>
  ///                                 | <level-statement>
  ///                                 | <assignment-init>
  std::unique_ptr<LUCOL_AST> parse_external_declaration();

  /// @brief Constructs an AST node for an integer literal
  /// @note: <literal> ::= <int-const>
  ///                    | <hex-const>
  /// <int-const> ::= "[0-9]+"
  /// <hex-const> ::= ">[0-9A-F]{1,4}"
  /// @pre: cur_tok must be Token::literal
  std::unique_ptr<LUCOL_AST> parse_literal();

  /// @brief Constructs an AST node for an variable reference
  /// @note: <identifier> ::= "[A-Z][A-Z0-9]{0,5}"
  ///                       | "\*"
  /// @pre: cur_tok must be Token::identifier
  /// @param: is_lucif - set true if the variable is the implicit register
  std::unique_ptr<LUCOL_AST> parse_variable(bool is_ptr = false, bool is_lucif = false);

  /// @brief Constructs an AST node for a subroutine call
  /// @note: <subroutine-statement> ::= "CALL" "(" <identifier> ")"
  /// @pre: cur_tok must be Token::subroutine_call
  std::unique_ptr<LUCOL_AST> parse_subroutine_statement();

  /// @brief Constructs an AST node for a LUCOL module call
  /// @note: <module-statement> ::= <identifier> "(" <ident-list> ")"
  /// @pre: cur_tok must be Token::identifier
  std::unique_ptr<LUCOL_AST> parse_module_statement();

  /// @brief Constructs an AST node for a LUCOL statement
  /// @note: <statement> ::= <condition-statement>
  ///                      | <case-statement>
  ///                      | <subroutine-statement>
  ///                      | <module-statement>
  std::unique_ptr<LUCOL_AST> parse_statement();

  /// @brief Constructs an AST node for a LUCOL subroutine definition
  /// @note: <subroutine-definition> ::= <subroutine-start> <identifier> <statement-list> <subroutine-end>
  /// @pre: cur_tok must be Token::subroutine_start
  std::unique_ptr<LUCOL_AST> parse_subroutine_definition();

  /// @brief Constructs an AST node for a variable declaration
  /// @note: <variable-declaration> ::= <type-specifier> <identifier> <direct-declarator>
  ///<type-specifier> ::= "VARIABLE"
  ///                   | "LOGICAL"
  ///                   | "CONSTANT"
  ///<direct-declarator> ::= "[" {<literal>} "]"
  ///                      | "(" <ident-list> ")"
  ///                      | "(" <literal-list> ")"
  ///                      | <epsilon>
  /// @pre: cur_tok must be Token::variable_def
  std::unique_ptr<LUCOL_AST> parse_variable_declaration();

  /// @brief Constructs an AST node for an if-then-else block
  /// @note: <condition-statement> ::= "DOIF" "(" <identifier> ")" <statement-list> "ELSE" <statement-list> "ENDIF"
  /// @pre: cur_tok must be Token::if_cond
  std::unique_ptr<LUCOL_AST> parse_condition_statement();

  /// @brief Constructs an AST node for a switch-case block
  /// @note: <case-statement> ::= "CASE" "(" <ident-list> ")" <case-block-list> "ENDCASE"
  /// @pre: cur_tok must be Token::case_init
  std::unique_ptr<LUCOL_AST> parse_case_statement();

  /// @brief Constructs an AST node for a level statement
  /// @note: <level-statement> ::= "LEVEL" <literal> <exec-specifier> "(" <ident-list> ")"
  /// @pre: cur_tok must be Token::level
  std::unique_ptr<LUCOL_AST> parse_level_statement();

  /// @brief Constructs an AST node for an assignment initialization statement
  /// @note: <assignment-init> ::= <ident> "=" <literal>
  /// @pre: cur_tok must be Token::identifier
  std::unique_ptr<LUCOL_AST> parse_assignment_statement();
};

#endif