// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include "LUCOL.h"
#include "LUCOL_Parser.h"
#include "LUCOL_Error.h"

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_external_declaration()
{
  switch (lex.get_cur_tok())
  {
    case Token::newline:
      lex(); // If we hit the end of a line then ignore and move to next token
      break;
    case Token::eof:
      break; // Nothing else to do if we're at the end of the file
    case Token::variable_def:
      return parse_variable_declaration();
      break;
    case Token::subroutine_start:
      return parse_subroutine_definition();
      break;
    case Token::level:
      return parse_level_statement();
      break;
    case Token::identifier:
      return parse_assignment_statement();
      break;
    case Token::error:
    default:
    throw LUCOL_Parse_Error("Incorrect token detected");
      // level statements not yet supported
      return nullptr;
      break;
  }
  // Shouldn't be making it out of here but just in case...
  return nullptr;
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_literal()
{
  auto result = backwards::make_unique<LUCOL_AST_Literal>(lex.get_literal());
  lex();
  return std::move(result);
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_variable(bool is_ptr, bool is_lucif)
{
  string name = (is_lucif) ? LUCIF_NAME : lex.get_ident();
  lex(); // Consume the identifier
  return backwards::make_unique<LUCOL_AST_Variable>(name, is_ptr);
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_subroutine_statement()
{
  if (lex() != Token::lparen) // consume the CALL, now expecting (
  {
    throw LUCOL_Parse_Error("Expecting ( after CALL statement");
  }
  
  if (lex() != Token::identifier) // consume the (, now expecting the subroutine name
  {
    throw LUCOL_Parse_Error("Expecting identifier after CALL ( statement");
  }
  // Grab the identifier - it's the subroutine name
  string subroutine_name = lex.get_ident();
  
  if (lex() != Token::rparen) // consume the ident, now expecting )
  {
    throw LUCOL_Parse_Error("Expecting ) after CALL ( <ident> statement");
  }
  
  lex(); // Consume the ) and return
  return backwards::make_unique<LUCOL_AST_Call>(subroutine_name);
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_module_statement()
{
  string module_name = lex.get_ident();
  if (lex() != Token::lparen) // consume the module name, now expecting (
  {
    throw LUCOL_Parse_Error("Expecting ( after LUCOL module call");
  }
  
  std::vector<std::unique_ptr<LUCOL_AST>> args;
  while (true)
  {
     // Consume the ( and see what's next
    Token cur_tok = lex();
    
    // Argument is a variable
    if (cur_tok == Token::identifier)
    {
      // Is just a pointer
      args.push_back(std::move(parse_variable(true)));
    }
    
    // Argument is the implicit register "*"
    else if (cur_tok == Token::lucif)
    {
      // Is both a pointer and the implicit register
      args.push_back(std::move(parse_variable(true, true)));
    }
    
    // Argument is a literal
    else if (cur_tok == Token::literal)
    {
      args.push_back(std::move(parse_literal()));
    }
    
    else if (cur_tok == Token::colon)
    {
      // TODO: Should we make a parse_terminator() function
      args.push_back(backwards::make_unique<LUCOL_AST_Terminator>());
      lex(); // Consume the colon and move on
    }
    
    // The literal/variable argument has been consumed, check what comes next
    cur_tok = lex.get_cur_tok();
    
    // List of arguments is over
    if (cur_tok == Token::rparen)
    {
      lex(); // Consume the rparen
      break;
    }
    
    // If the list isn't over, a comma needs to be next
    else if (cur_tok != Token::comma)
    {
      throw LUCOL_Parse_Error("Expected comma-delimited argument list");
    }
  }
  return backwards::make_unique<LUCOL_AST_Call>(module_name, std::move(args)); 
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_statement()
{
  switch (lex.get_cur_tok())
  {
    case Token::newline:
      lex(); // If we hit the end of a line then ignore and move to next token
      break;
    case Token::identifier:
      return parse_module_statement();
      break;
    case Token::subroutine_call:
      return parse_subroutine_statement();
      break;
    case Token::if_cond:
      return parse_condition_statement();
      break;
    case Token::case_init:
      return parse_case_statement();
      break;
    case Token::variable_def:
      return parse_variable_declaration();
      break;
    case Token::error:
    default:
      throw LUCOL_Parse_Error("Incorrect token detected");
      return nullptr;
      break;
  }
  // Shouldn't be making it out of here but just in case...
  return nullptr;
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_subroutine_definition()
{  
  if (lex() != Token::identifier)
  {
    throw LUCOL_Parse_Error("Expected identifier after SEGMENT/PROCEDURE keyword");
  }
  string name = lex.get_ident();
  
  lex(); // Consume the segment name
  
  auto prototype = backwards::make_unique<LUCOL_AST_Prototype>(name);
  // Get the definition somehow....
  std::vector<std::unique_ptr<LUCOL_AST>> statements;
  
  while(lex.get_cur_tok() != Token::subroutine_end)
  {
    auto statement = parse_statement();
    if (statement) // Make sure it didn't return nullptr
    {
      statements.push_back(std::move(statement));
    }
  }
  
  lex(); // Consume the subroutine end token and return
  
  return backwards::make_unique<LUCOL_AST_Definition>(std::move(prototype), std::move(statements));
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_variable_declaration()
{
  LUCOL_Type type;
  bool is_alias = false;
  int array_size = 0;
  if (lex.get_ident() == "VARIABLE")
  {
    type = LUCOL_Type::VARIABLE;
  }
  
  else if (lex.get_ident() == "CONSTANT")
  {
    type = LUCOL_Type::CONSTANT;
  }
  
  else if (lex.get_ident() == "LOGICAL")
  {
    type = LUCOL_Type::LOGICAL;
  }
  
  // TODO: long variables
  if (lex() != Token::identifier)
  {
    throw LUCOL_Parse_Error("Type specifiers must be followed by a variable name");
  }
  string name = lex.get_ident();
  
  std::vector<std::unique_ptr<LUCOL_AST>> initializers;
  
  lex(); // Consume the identifier
  
  // There's some sort of initializer
  if (lex.get_cur_tok() == Token::lparen)
  {
    // TODO: Multiple initializers
    Token initializer_type = lex(); 
    // Should be literal (standard array) or identifier (alias array)
    if ((initializer_type != Token::literal) && (initializer_type != Token::identifier))
    {
      throw LUCOL_Parse_Error("Initializer must be a literal or a variable");
    }
    
    while (true)
    {
      if (lex.get_cur_tok() != initializer_type)
      {
        throw LUCOL_Parse_Error("Initializer must be all literals or all variables");
      }
      
      if (lex.get_cur_tok() == Token::literal)
      {
        // It's a literal initializer...
        initializers.push_back(parse_literal());
      }
      
      else if (lex.get_cur_tok() == Token::identifier)
      {
        // It's an alias instead of a variable...
        // TODO: Aliasing - don't want to allocate for "shallow" variables
        // QUESTION: Will this work for arrays?
        // Must specify pointer so IR gen won't create a load instruction (i.e. 
        // so it just returns the pointer with which an alias can be created)
        initializers.push_back(parse_variable(true));
        is_alias = true;
      }
      
      // Check the next token (should be comma or rparen)
      // End of initializer has just been consumed - terminated by ) - so we assume that the 
      // ending ) has been read in
      if (lex.get_cur_tok() == Token::rparen)
      {
        break;
      }
      
      // If it's a comma, just consume the comma and move on
      else if (lex.get_cur_tok() == Token::comma)
      {
        lex();
      }
      
      // Otherwise something has gone wrong
      else
      {
        throw LUCOL_Parse_Error("Initializer must be comma-delimited and end with )");
      }
    }
    
    if (initializers.empty())
    {
      return nullptr;
    }
    
    lex(); // Consume the ) now that we've verified it
  }
  
  // Array initialization
  else if (lex.get_cur_tok() == Token::lbracket)
  {
    if (lex() != Token::literal) // Array size specifier
    {
      throw LUCOL_Parse_Error("Array initializers must be a literal");
    }

    // Pull the number directly from the lexer
    array_size = lex.get_literal();
    lex(); //consume literal
    if (lex.get_cur_tok() != Token::rbracket)
    {
      throw LUCOL_Parse_Error("Array initializer must end with ]");
    }
    
    lex(); // Consume the ] now that it's been verified
  }
  
  else if (lex.get_cur_tok() == Token::equals)
  {
    lex(); // Consume the equals
    if (lex.get_cur_tok() == Token::literal)
    {
      // It's a literal initializer...
      initializers.push_back(parse_literal());
    }
    
    else
    {
      throw LUCOL_Parse_Error("Assignment RHS in variable declaration must be a literal");
    }
  }
  
  return backwards::make_unique<LUCOL_AST_Declaration>(std::make_pair(name, std::move(initializers)), type, is_alias, array_size);
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_condition_statement()
{
  if (lex() != Token::lparen) // Consume the DOIF and grab the (
  {
    throw LUCOL_Parse_Error("DOIF must be followed by (");
  }
  
  if (lex() != Token::identifier) // Consume the ( and grab the variable
  {
    throw LUCOL_Parse_Error("DOIF must include condition variable");
  }
  // Grab the variable name and construct the AST node
  auto cond = parse_variable();
  
  if (lex.get_cur_tok() != Token::rparen)
  {
    throw LUCOL_Parse_Error("DOIF condition must be followed by )");
  }
  
  lex(); // Consume the rparen
  
  std::vector<std::unique_ptr<LUCOL_AST>> if_block;
  
  // Grab the contents of the "if" block
  while ((lex.get_cur_tok() != Token::else_cond) && (lex.get_cur_tok() != Token::endif))
  {
    auto statement = parse_statement();
    if (statement) // Make sure it didn't return nullptr
    {
      if_block.push_back(std::move(statement));
    }
  }
  
  std::vector<std::unique_ptr<LUCOL_AST>> else_block;
  
  if (lex.get_cur_tok() == Token::else_cond)
  {
    lex(); // Consume the ELSE
    // Grab the contents of the "if" block
    while (lex.get_cur_tok() != Token::endif)
    {
      auto statement = parse_statement();
      if (statement) // Make sure it didn't return nullptr
      {
        else_block.push_back(std::move(statement));
      }
    }
  }
  
  lex(); // Consume the ENDIF
  return backwards::make_unique<LUCOL_AST_Conditional>(std::move(cond), std::move(if_block), std::move(else_block));
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_case_statement()
{
  if (lex() != Token::lparen) // Consume the DOIF and grab the (
  {
    throw LUCOL_Parse_Error("CASE must be followed by (");
  }
  
  if (lex() != Token::identifier) // Consume the ( and grab the variable
  {
    throw LUCOL_Parse_Error("CASE must include condition variable");
  }
  
  // Grab the variable name and construct the AST node for the condition (the switch/index)
  auto cond = parse_variable();
  
  // At this point the variable has been consumed so we're expecting a comma or a )
  std::unordered_map<string, std::vector<std::unique_ptr<LUCOL_AST>>> case_blocks;
  std::vector<std::unique_ptr<LUCOL_AST>> default_block;
  
  // Mapping of identifiers to the corresponding values of the condition variable
  std::unordered_map<string, std::vector<unsigned int>> case_map; 
  
  unsigned int num_cases = 0; // How many cases to expect
  // Loop for reading initial CASE statement
  while (true)
  {
    Token cur_tok = lex.get_cur_tok();
    
    // List of arguments is over
    if (cur_tok == Token::rparen)
    {
      lex(); // Consume the )
      break;
    }
    
    // If the list isn't over, a comma needs to be next
    else if (cur_tok != Token::comma)
    {
      throw LUCOL_Parse_Error("Expected comma-delimited argument list");
    }
    
    lex(); // Consume the comma
    
    if (lex.get_cur_tok() == Token::identifier)
    {
      // If we find an identifier, consume it and increment the number of cases
      // Denoting that the current case identifier corresponds to the current
      // case number
      case_map[lex.get_ident()].push_back(num_cases);
      num_cases++;
    }
    
    // Colons are used to indicate the end of a variadic list (list terminator)
    // We can ignore them for now since we already have logic to exit this loop
    else if (lex.get_cur_tok() != Token::colon)
    {
      throw LUCOL_Parse_Error("Contents of CASE statement must be valid identifiers");
    }
    
    lex(); // Consume the identifier or colon
  }
  
  bool is_default = false; // OTHERWISE flag
  
  // Outer loop for entire case statement (should run num_cases times)
  while (lex.get_cur_tok() != Token::case_end)
  {
    std::vector<std::unique_ptr<LUCOL_AST>> case_block;
    
    string case_name;
    
    // We only care about checking all this for explicit cases 
    // Default case only has OTHERWISE keyword which would be 
    // the current token here if applicable
    if (!is_default)
    {
      if (lex.get_cur_tok() != Token::case_entry)
      {
        throw LUCOL_Parse_Error("Case statement requires BEGIN statements");
      }
      
      if (lex() != Token::lparen) // Consume the BEGIN, should grab a (
      {
        throw LUCOL_Parse_Error("BEGIN should be followed by (");
      }
      
      if (lex() != Token::identifier) // Consume the (, then there should be an ident e.g. CASE0
      {
        throw LUCOL_Parse_Error("BEGIN should be followed by identifier");
      }
      
      case_name = lex.get_ident(); // Grab the name of the case
      
      if (!case_map.count(case_name))
      {
        throw LUCOL_Parse_Error("Case block not found in CASE initializer");
      }
      
      if (lex() != Token::rparen) // Consume the ident, should be a )
      {
        throw LUCOL_Parse_Error("BEGIN statement identifier must be enclosed in parentheses");
      }
      
      lex(); // Consume the rparen
    }
    
    // Inner loop for individual cases (should run once per statement in block)
    while (true)
    {
      Token cur_tok = lex.get_cur_tok();
      // End of case statement
      if ((cur_tok == Token::case_entry) || (cur_tok == Token::case_end) || (cur_tok == Token::case_default))
      {
        // All of these mean the current case is finished so let's save it off
        if (!is_default)
        {
          case_blocks[case_name] = std::move(case_block);
        }
        
        // TODO: Should we clear the default flag if we hit a case_entry after the OTHERWISE?
        
        // Set the default flag if applicable
        if (cur_tok == Token::case_default)
        {
          is_default = true;
          if (!default_block.empty())
          {
            throw LUCOL_Parse_Error("Only a single OTHERWISE block is allowed");
          }
          lex(); // Consume the OTHERWISE
        }
        // Finally break out of the inner loop so the outer loop can check for case_end
        break;
      }
      
      auto statement = parse_statement();
      
      if (is_default)
      {
        default_block.push_back(std::move(statement));
      }
      
      else
      {
        case_block.push_back(std::move(statement));
      }
    }  
  }
  
  lex(); // Consume the ENDCASE
  
  // if (case_blocks.size() != num_cases)
  // {
  //   throw LUCOL_Parse_Error("Number of cases defined does not match CASE initializer");
  // }
  
  return backwards::make_unique<LUCOL_AST_Case>(std::move(cond), std::move(case_blocks), std::move(default_block), std::move(case_map));
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_level_statement() // return void?
{
  if (lex() != Token::literal)
  {
    throw LUCOL_Parse_Error("Interrupt specifier must use a literal");
  }
  
  short level = lex.get_literal(); // Grab the # of the level we care about
  
  lex(); // Consume the level integer and grab the PWRUP/EXEC
  
  if (((level == 0) && (lex.get_cur_tok() != Token::pwrup)) || ((level > 0) && (lex.get_cur_tok() != Token::exec)))
  {
    throw LUCOL_Parse_Error("Interrupt level 0 requires trailing PWRUP, all other levels require trailing EXEC");
  }
  
  if (lex() != Token::lparen)
  {
    throw LUCOL_Parse_Error("Expected '(' after PWRUP/EXEC");
  }
  
  std::vector<std::unique_ptr<LUCOL_AST>> calls;
  
  lex(); // Consume the ( and grab the first segment
  
  while (true)
  {
    Token cur_tok = lex.get_cur_tok();
    
    // List of arguments is over
    if  (cur_tok == Token::rparen)
    {
      lex(); // Consume the rparen
      break;
    }
    
    else if (cur_tok == Token::identifier)
    {
      // If we find an identifier...
      string subroutine_name = lex.get_ident();
      calls.push_back(backwards::make_unique<LUCOL_AST_Call>(subroutine_name));
    }
    
    // If the list isn't over, a comma needs to be next
    else if (cur_tok != Token::comma)
    {
      throw LUCOL_Parse_Error("Contents of PWRUP/EXEC statement must be parentheses-enclosed list of comma-delimited identifiers");
    }
    
    lex(); // Consume the , or ident
  }
  
  // Functions have name _level0, _level1, etc
  auto prototype = backwards::make_unique<LUCOL_AST_Prototype>("_level" + std::to_string(level));
  
  return backwards::make_unique<LUCOL_AST_Definition>(std::move(prototype), std::move(calls));
}

std::unique_ptr<LUCOL_AST> LUCOL_Parser::parse_assignment_statement()
{
  LUCOL_Type type = LUCOL_Type::CONSTANT; // Only constants can be initialized with "="
  string name = lex.get_ident();
  
  // Consume the identifier, expecting an equals sign now
  if (lex() != Token::equals)
  {
    throw LUCOL_Parse_Error("External assignment statement must contain \"=\"");
  }
  
  lex(); // Consume the '='
  
  std::vector<std::unique_ptr<LUCOL_AST>> initializers;
  
  // Now expecting a literal of some sort
  initializers.push_back(parse_literal());
  
  return backwards::make_unique<LUCOL_AST_Declaration>(std::make_pair(name, std::move(initializers)), type);
}
