// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include <cctype>
#include <cstdio>

#include "LUCOL_Lexer.h"

// Initialize the file position struct
string LUCOL_File_Position::file_name = "";
// Line and column numbers are generally 1-indexed
unsigned int LUCOL_File_Position::line_num = 1;
unsigned int LUCOL_File_Position::col_num = 1;

// "^[A-Z][A-Z0-9]{0,5}$"
bool no_regex::match_ident(string token)
{
  unsigned char max_tok = 15;
  if ((token.length() < 1) || (token.length() > max_tok))
    return false;
  if (!isupper(token[0]))
    return false;
  for (unsigned int i = 1; i < token.length(); i++)
    if (!isalnum(token[i]))
      return false;
  return true;
}

// "^[+-]?[0-9]+$"
bool no_regex::match_int(string token)
{
  unsigned char start = ((token[0] == '+') || (token[0] == '-')) ? 1 : 0;
  if (token.length() < (1U + start))
    return false;
  for (unsigned int i = start; i < token.length(); i++)
    if (!isdigit(token[i]))
      return false;
  return true;
}

// "^>[0-9A-F]{1,4}$"
bool no_regex::match_hex(string token)
{
  if (token.length() > 5)
    return false;
  if (token[0] != '>')
    return false;
  for (unsigned int i = 1; i < token.length(); i++)
    if (!isxdigit(token[i]))
      return false;
  return true;
}

char LUCOL_Lexer::get_next_char() const
{
  LUCOL_File_Position::col_num++;
  return input_stream.get();
}

Token LUCOL_Lexer::ignore_line()
{
  do
  {
    curr_char = get_next_char();
  } while ((curr_char != '\n') && (curr_char != '\r') && (curr_char != EOF));

  if (curr_char != EOF)
  {
    // Try again to find a token
    return (*this)();
  }

  else
  {
    return Token::eof;
  }
}

Token LUCOL_Lexer::grab()
{
  // Consume any whitespace preceding the token
  while (isspace(curr_char))
  {
    // Process the newline if applicable
    if (curr_char == '\n')
    {
      LUCOL_File_Position::line_num++;
      LUCOL_File_Position::col_num = 1;
    }
    curr_char = get_next_char();
  }

  // Then check for identifiers and keywords
  // [A-Z][A-Z0-9]{0,5} or [0-9]+ or >[0-9A-F]{1,4}
  ident = "";
  while (isupper(curr_char) || isdigit(curr_char) || curr_char == '>' || curr_char == '-' || curr_char == '+')
  {
    ident += curr_char;
    curr_char = get_next_char();
  }
  //std::cout<<"Ident is: "<<ident<<std::endl;
  if ((ident == "SEGMENT") || (ident == "PROCEDURE"))
  {
    return Token::subroutine_start;
  }

  else if ((ident == "ENDSEG") || (ident == "RETURN"))
  {
    return Token::subroutine_end;
  }

  else if ((ident == "VARIABLE") || (ident == "CONSTANT") || (ident == "LOGICAL"))
  {
    return Token::variable_def;
  }

  else if (ident == "CALL")
  {
    return Token::subroutine_call;
  }

  else if (ident == "DOIF")
  {
    return Token::if_cond;
  }

  else if (ident == "ELSE")
  {
    return Token::else_cond;
  }

  else if (ident == "ENDIF")
  {
    return Token::endif;
  }

  else if (ident == "CASE")
  {
    return Token::case_init;
  }

  else if (ident == "BEGIN")
  {
    return Token::case_entry;
  }

  else if (ident == "OTHERWISE")
  {
    return Token::case_default;
  }

  else if (ident == "ENDCASE")
  {
    return Token::case_end;
  }

  else if (ident == "LEVEL")
  {
    return Token::level;
  }

  else if (ident == "PWRUP")
  {
    return Token::pwrup;
  }

  else if (ident == "EXEC")
  {
    return Token::exec;
  }

  // TODO: Actually implement the documentation information - not for FS19
  else if (ident == "TITLE")
  {
    return ignore_line();
  }

  else if (ident == "SUBTITLE")
  {
    return ignore_line();
  }
  // Other keywords go here

  // An actual identifier!!
  else if (no_regex::match_ident(ident))
  {
    return Token::identifier;
  }
  // An int const
  else if (no_regex::match_int(ident))
  {
    literal_val = std::stoi(ident);
    return Token::literal;
  }
  // A hex const
  else if (no_regex::match_hex(ident))
  {
    ident.erase(ident.begin());
    literal_val = std::stoul(ident, nullptr, 16);
    return Token::literal;
  }
  // A comment
  else if (curr_char == ';')
  {
    return ignore_line();
  }

  else if (curr_char == EOF)
  {
    return Token::eof;
  }

  // Single-character tokens
  char prev_char = curr_char;
  curr_char = get_next_char();
  //std::cout<<"Previous char is: "<<prev_char<<std::endl;
  //std::cout<<"Current char is: "<<curr_char<<std::endl;
  if (prev_char == '(')
  {
    return Token::lparen;
  }

  else if (prev_char == ')')
  {
    return Token::rparen;
  }

  if (prev_char == '[')
  {
    return Token::lbracket;
  }

  else if (prev_char == ']')
  {
    return Token::rbracket;
  }

  else if (prev_char == ',')
  {
    return Token::comma;
  }

  else if (prev_char == '*')
  {
    return Token::lucif;
  }

  else if (prev_char == '=')
  {
    return Token::equals;
  }

  else if (prev_char == ':')
  {
    return Token::colon;
  }

  else if ((prev_char == '\n') || (prev_char == '\r'))
  {
    if (prev_char == '\n')
    {
      LUCOL_File_Position::line_num++;
      LUCOL_File_Position::col_num = 1;
    }
    return Token::newline;
  }

  return Token::error;
}

Token LUCOL_Lexer::operator()()
{
  cur_tok = grab();
  return cur_tok;
}
