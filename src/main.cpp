// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include "LUCOL_Lexer.h"
#include "LUCOL_AST.h"
#include "LUCOL_Parser.h"
#include "LUCOL_IR.h"
#include "LUCOL_Debug.h"

#include <iostream>
#include <fstream>
#include <unistd.h>

// Needed to build object files
#include <llvm/ADT/Optional.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/raw_os_ostream.h> // For outputting the IR (.ll) file
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Target/TargetOptions.h>

using std::cout;
using std::endl;

using namespace llvm;

int generate_object_file(string obj_file)
{
  // Get the architecture target - e.g. x86_64-unknown-linux-gnu
  auto target_key = sys::getDefaultTargetTriple();

  // Initialize the LLVM backend...
  InitializeAllTargetInfos();
  InitializeAllTargets();
  InitializeAllTargetMCs();
  InitializeAllAsmParsers();
  InitializeAllAsmPrinters();

  // ..then grab a pointer to the desired architecture
  string err;
  auto target = TargetRegistry::lookupTarget(target_key, err);

  if (!target)
  {
    cout << err;
    return -1;
  }

  auto cpu_type = "generic"; // Will want to select the processor that the iHawk box uses
  auto features = "";        // Several hundred available, probably won't need anything like FMA, SSE, etc

  TargetOptions opt;
  auto reloc_model = Optional<Reloc::Model>(Reloc::Model::PIC_); // Position-independent execution by default
  std::unique_ptr<TargetMachine> target_machine(target->createTargetMachine(target_key, cpu_type, features, opt, reloc_model));

  // Let the front-end know what we're compiling to as a hint to the optimizer
  // (in case we care about optimizations)
  LUCOL_IR_Generator::module->setDataLayout(target_machine->createDataLayout());
  LUCOL_IR_Generator::module->setTargetTriple(target_key);

  // Open the file
  std::error_code err_code;
  raw_fd_ostream dest(obj_file, err_code, sys::fs::F_None);

  if (err_code)
  {
    cout << "Could not open object file for writing: " << err_code.message();
    return -2;
  }

  legacy::PassManager pass;
  auto object_type = TargetMachine::CGFT_ObjectFile;

#ifndef LLVM7
  if (target_machine->addPassesToEmitFile(pass, dest, object_type))
  {
#else
  if (target_machine->addPassesToEmitFile(pass, dest, NULL, object_type))
  {
#endif
    errs() << "The target machine can't write to this kind of file";
    return -3;
  }

  pass.run(*LUCOL_IR_Generator::module);
  dest.flush();
  return 0;
}

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    throw std::runtime_error("Pass the name of the file to compile");
  }

  string prototype_path = "";

  char opt;
  while ((opt = getopt(argc, argv, "wp:")) != -1)
  {
    switch (opt)
    {
    case 'w':
      LUCOL_IR_Generator::is_define_file = false;
      break;
    case 'p':
      prototype_path = optarg;
      break;
    case '?':
      throw std::runtime_error("Unknown command-line argument received");
      break;
    }
  }

  string src_file_arg = argv[optind];

  if (src_file_arg.length() < 4)
  {
    throw std::runtime_error("Filename is too short");
  }

  string src_file_no_ext = src_file_arg.substr(0, src_file_arg.length() - 4);

  LUCOL_File_Position::file_name = src_file_arg;
  std::ifstream src_file(LUCOL_File_Position::file_name);

  if (!src_file.good())
  {
    throw std::runtime_error("Could not open source file - make sure the file exists");
  }

  LUCOL_Parser p(src_file);
  LUCOL_IR_Generator::module = backwards::make_unique<Module>(LUCOL_File_Position::file_name, LUCOL_IR_Generator::context);

  // Initialize the debug stuff
  LUCOL_Debug::dbg_builder = backwards::make_unique<DIBuilder>(*LUCOL_IR_Generator::module.get());
  DWARF_Info::compile_unit = LUCOL_Debug::dbg_builder->createCompileUnit(
      dwarf::DW_LANG_C, LUCOL_Debug::dbg_builder->createFile(LUCOL_File_Position::file_name, "."),
      "LUCOL Compiler", 0, "", 0);

  // This is technically optional
  if (prototype_path.length() > 0)
  {
    generate_prototypes(prototype_path);
  }

  p.initialize();

  while (true)
  {
    try
    {
      auto AST_node = p.parse_external_declaration();
      if (!AST_node)
      {
        break;
      }

      auto generated_ir = AST_node->generate_ir();
      if (!generated_ir)
      {
        break;
      }
    }

    catch (std::exception &e)
    {
      cout << e.what() << endl;
      src_file.close();
      return -1;
    }
  }

  src_file.close();

  // Finalize the debugging info
  LUCOL_Debug::dbg_builder->finalize();

  // Print the full generated IR to file
  // via llvm::raw_ostream, not sure if this is compatible with STL streams
  std::ofstream ir_file(src_file_no_ext + ".ll");
  raw_os_ostream raw_ir_file(ir_file);
  LUCOL_IR_Generator::module->print(raw_ir_file, nullptr);
  ir_file.close();

  generate_object_file(src_file_no_ext + ".o");
  return 0;
}