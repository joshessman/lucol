// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include "LUCOL_AST.h"

#include <iostream>

using std::cout;
using std::endl;

// TODO: Build into logger class
std::unique_ptr<LUCOL_AST> error(string err)
{
  cout << err << endl;
  return nullptr;
}
