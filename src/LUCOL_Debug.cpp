// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include "LUCOL_Debug.h"
#include "LUCOL_IR.h"    // For ir_builder
#include "LUCOL_Lexer.h" // For LUCOL_File_Position

std::unique_ptr<DIBuilder> LUCOL_Debug::dbg_builder;
DICompileUnit *DWARF_Info::compile_unit;
DIType *DWARF_Info::i8_type;
DIType *DWARF_Info::i8_ptr_type;
DIType *DWARF_Info::i16_type;
DIType *DWARF_Info::i16_ptr_type;
std::vector<DIScope *> DWARF_Info::scope_blocks;

DIType *DWARF_Info::get_i8_type()
{
  if (!i8_type)
  {
    i8_type = LUCOL_Debug::dbg_builder->createBasicType("LOGICAL", 8, dwarf::DW_ATE_signed);
  }
  return i8_type;
}

DIType *DWARF_Info::get_i8_ptr_type()
{
  if (!i8_ptr_type)
  {
    // Number of bits in a pointer to variable is sizeof(short*) * 8
    i8_ptr_type = LUCOL_Debug::dbg_builder->createPointerType(get_i8_type(), sizeof(short *) << 3);
  }
  return i8_ptr_type;
}

DIType *DWARF_Info::get_i16_type()
{
  if (!i16_type)
  {
    i16_type = LUCOL_Debug::dbg_builder->createBasicType("VARIABLE", 16, dwarf::DW_ATE_signed);
  }
  return i16_type;
}

DIType *DWARF_Info::get_i16_arr_type()
{
  // FIXME: Hash table for different sizes
  // if (!i16_arr_type)
  // {
  //   // i16_arr_type = LUCOL_Debug::dbg_builder->createBasicType("VARIABLE[]", 16, dwarf::DW_ATE_signed);
  // }
  // return i16_arr_type;
}

DIType *DWARF_Info::get_i16_ptr_type()
{
  if (!i16_ptr_type)
  {
    // Number of bits in a pointer to variable is sizeof(short*) * 8
    i16_ptr_type = LUCOL_Debug::dbg_builder->createPointerType(get_i16_type(), sizeof(short *) << 3);
  }
  return i16_ptr_type;
}

// TODO: Read source info from variable stored in AST node??
void DWARF_Info::emit_location(LUCOL_Source_Info loc)
{
  DIScope *scope;
  if (scope_blocks.empty())
  {
    scope = compile_unit;
  }
  else
  {
    scope = scope_blocks.back();
  }
  LUCOL_IR_Generator::ir_builder.SetCurrentDebugLocation(
      DebugLoc::get(loc.line, loc.col, scope));
}
//
// DIType* DWARF_Info::get_void_type()
// {
//   // if (!void_type)
//   // {
//   //   void_type = LUCOL_Debug::dbg_builder->createBasicType("VOID", 0, dwarf::DW_ATE_void);
//   // }
//   return void_type;
// }