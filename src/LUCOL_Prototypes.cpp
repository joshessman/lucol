// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include <fstream>
#include <iterator>

#include "LUCOL.h"
#include "LUCOL_AST.h"
#include "LUCOL_Error.h"

void generate_prototypes(const string &proto_path)
{
  // QUESTION Do we need to handle underscores for the module names?

  std::ifstream proto_file(proto_path);

  if (!proto_file.good())
  {
    throw std::runtime_error("Could not open prototype file");
  }

  // Split the file on whitespace
  std::vector<string> parsed_protos((std::istream_iterator<string>(proto_file)),
                                    std::istream_iterator<string>());
  proto_file.close();

  unsigned int i = 0;

  while (i < parsed_protos.size())
  {
    bool variadic = false;

    // First check to see if the function is variadic
    if (parsed_protos.at(i) == "VARIADIC")
    {
      variadic = true;
      i++;
    }

    // The name of the module is the first bit of the prototype
    std::string module_name = parsed_protos.at(i);
    i++;

    std::vector<string> arg_types;
    while (parsed_protos.at(i) != "$") // $ is the end of the prototype
    {
      arg_types.push_back(parsed_protos.at(i)); // The type of each argument
      i++;
    }
    i++;

    // Construct the module AST node for the prototype and generate its IR
    auto LUCOL_module = backwards::make_unique<LUCOL_AST_Prototype>(module_name, arg_types, variadic);
    if (!LUCOL_module->generate_ir())
    {
      throw LUCOL_IR_Error("Failed to generate IR for \"" + module_name + " \" module prototype");
    }
  }

  // Mutable 2-byte variable -> VARIABLE
  auto LUCIF = backwards::make_unique<LUCOL_AST_Declaration>(std::make_pair(LUCIF_NAME, std::vector<std::unique_ptr<LUCOL_AST>>()), LUCOL_Type::VARIABLE);
  if (!LUCIF->generate_ir())
  {
    throw LUCOL_IR_Error("Failed to generate IR for LUCOL implicit register declaration");
  }
}