// An LLVM frontend for LUCOL, the LUcas COntrol Language
// Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
// The full license for this code can be found in the root directory
// of this repository in a file called LICENSE.

#include "LUCOL_AST.h"
#include "LUCOL_Error.h"
#include "LUCOL_Debug.h"
#include "LUCOL_IR.h"

// Initialization of static singleton variables
LLVMContext LUCOL_IR_Generator::context;
IRBuilder<> LUCOL_IR_Generator::ir_builder(LUCOL_IR_Generator::context);
// Defaulting to LUCOL but in reality the module will get re-initialized
// with the name of the segment - though modules as a general rule do not share globals
// so potentially this could be the build name??
std::unique_ptr<Module> LUCOL_IR_Generator::module;
std::map<std::string, LUCOL_Symbol> LUCOL_IR_Generator::symbol_table;
// Storage for local variables
// FIXME: Can this be combined with symbol_table into a stack??
std::map<std::string, std::map<std::string, LUCOL_Symbol>> LUCOL_IR_Generator::local_symbols;

// Probably will only be occupied by a single function...
std::stack<std::string> LUCOL_IR_Generator::function_stack;

bool LUCOL_IR_Generator::is_define_file = true;

Value *LUCOL_AST_Literal::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());
  // 16-bit signed integer
  return ConstantInt::get(LUCOL_IR_Generator::context, APInt(16, val, true));
}

Value *LUCOL_AST_Variable::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());
  Value *val;

  bool is_local = true;
  bool is_alias = false;

  // First check local scope (will hide globals)
  if ((!LUCOL_IR_Generator::function_stack.empty()) &&
      LUCOL_IR_Generator::local_symbols[LUCOL_IR_Generator::function_stack.top()].count(name))
  {
    val = LUCOL_IR_Generator::local_symbols[LUCOL_IR_Generator::function_stack.top()][name].val;
    is_alias = LUCOL_IR_Generator::local_symbols[LUCOL_IR_Generator::function_stack.top()][name].is_alias;
  }

  else if (LUCOL_IR_Generator::symbol_table.count(name))
  {
    // Meaning it wasn't found in the local symbol table
    is_local = false;
    // Then try to grab the pointer to the global variable
    val = LUCOL_IR_Generator::symbol_table[name].val;
    is_alias = LUCOL_IR_Generator::symbol_table[name].is_alias;
  }

  // Otherwise the variable must not exist
  else
  {
    // Even if this is a file with strong defintions,
    // we can't expect all its variables to be defined
    // The DEFINE file might try to alias a variable that has yet to be declared
    // It will be replaced by the last (most recent) definition so we can make
    // an incorrect "temporary" i.e. forward declaration here

    // Because no headers or externs exist, we assume that some other file
    // will provide us the definition or that it will be defined later on in the file.
    // If this assumption is incorrect we delay the error until link time.
    // Final argument is 'true' to indicate force_global - this pseudo-extern
    // declaration must be for a global variable - if it were local, we
    // wouldn't need to generate this pseudo-extern
    auto externed_var = backwards::make_unique<LUCOL_AST_Declaration>(
        std::make_pair(name, std::vector<std::unique_ptr<LUCOL_AST>>()),
        LUCOL_Type::VARIABLE, false, 0, true);
    val = externed_var->generate_ir();
    if (!val)
    {
      throw LUCOL_IR_Error("Failed to generate IR for pseudo-extern declaration");
    }
  }

  // TODO: Use ir_builder.createGEP() for array types (or createExtract)
  // TODO: Resolve aliases here using some alias structure (symboltable?)

  // Let the call handle the dereferencing - should always return a pointer to a variable...
  // ...which is why local aliases must still be dereferenced (would be double pointers)
  if (is_local && is_alias)
  {
    val = LUCOL_IR_Generator::ir_builder.CreateLoad(val, name.c_str());
  }
  return (is_ptr) ? val : LUCOL_IR_Generator::ir_builder.CreateLoad(val, name.c_str());
}

Value *LUCOL_AST_Terminator::generate_ir() const
{
  // FIXME: Shouldn't automatically be i16*??
  auto nullptr_type = PointerType::getUnqual(Type::getInt16Ty(LUCOL_IR_Generator::context));
  return ConstantPointerNull::get(nullptr_type);
}

Value *LUCOL_AST_Call::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());

  Function *func = LUCOL_IR_Generator::module->getFunction(name);
  if (!func)
  {
    throw LUCOL_IR_Error("Segment/procedure/module with name \"" + name + "\" does not exist");
  }

  if ((func->arg_size() != args.size()) && (!func->isVarArg()))
  {
    throw LUCOL_IR_Error("Argument number mismatch for function \"" + name +
                         " \" - expected " + std::to_string(func->arg_size()) +
                         ", got " + std::to_string(args.size()));
  }

  std::vector<Value *> pass_args; // Will always be empty for segment/procedure

  // All non-variadic arguments
  for (auto &func_arg : func->args())
  {
    auto curr_arg = args[func_arg.getArgNo()]->generate_ir();
    // If we don't want to pass a pointer and the argument was of variable type (meaning only a pointer was
    // passed), we need a load instruction to get at the underlying variable (a dereference)
    if ((func_arg.getName().back() != '*') && (dynamic_cast<LUCOL_AST_Variable *>(args[func_arg.getArgNo()].get())))
    {
      curr_arg = LUCOL_IR_Generator::ir_builder.CreateLoad(curr_arg);
    }

    pass_args.push_back(curr_arg);

    if (!pass_args.back())
    {
      return nullptr;
    }
  }

  // Any remaining variadic arguments
  for (unsigned int i = func->arg_size(); i < args.size(); i++)
  {
    // WARNING: We don't have the ability to create load instructions here -
    // hopefully we don't ever need to dereference
    pass_args.push_back(args[i]->generate_ir());

    if (!pass_args.back())
    {
      return nullptr;
    }
  }

  // Returns void
  return LUCOL_IR_Generator::ir_builder.CreateCall(func, pass_args);
}

Function *LUCOL_AST_Prototype::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());

  std::vector<Type *> arg_types(args.size());
  for (unsigned int i = 0; i < args.size(); i++)
  {
    if (args[i].back() == '*')
    {
      arg_types[i] = Type::getInt16PtrTy(LUCOL_IR_Generator::context);
    }

    else
    {
      arg_types[i] = Type::getInt16Ty(LUCOL_IR_Generator::context);
    }
  }

  // All LUCOL modules return void
  FunctionType *return_type = FunctionType::get(Type::getVoidTy(LUCOL_IR_Generator::context), arg_types, is_variadic);
  Function *func = Function::Create(return_type, Function::ExternalLinkage, name, LUCOL_IR_Generator::module.get());
  // Name the args for organizational reasons
  int i = 0;
  for (auto &arg : func->args())
  {
    arg.setName(args[i]);
    i++;
  }
  return func;
}

Function *LUCOL_AST_Definition::generate_ir() const
{
  DWARF_Info::emit_location(proto->get_loc());

  // First check to see if it's in the "symbol table"
  Function *func = LUCOL_IR_Generator::module->getFunction(proto->get_name());
  // If it's not, codegen the prototype
  if (!func)
  {
    func = proto->generate_ir();
  }
  // Check for prototype codegen error
  if (!func)
  {
    return nullptr;
  }
  // Don't allow multiple definitions
  if (!func->empty())
  {
    throw LUCOL_IR_Error("Segment/procedure redefinition prohibited for subroutine \"" + proto->get_name() + "\"");
  }

  BasicBlock *block = BasicBlock::Create(LUCOL_IR_Generator::context, "entry", func);
  LUCOL_IR_Generator::ir_builder.SetInsertPoint(block);

  // DWARF configuration for the segment
  DIFile *unit = LUCOL_Debug::dbg_builder->createFile(DWARF_Info::compile_unit->getFilename(),
                                                      DWARF_Info::compile_unit->getDirectory());
  DIScope *dbg_context = unit;

  // Is a vector to allow for arguments (of which there are none)
  SmallVector<Metadata *, 1> dbg_return_type;
  // dbg_return_type.push_back(DWARF_Info::get_void_type());
  auto type_list = LUCOL_Debug::dbg_builder->getOrCreateTypeArray(dbg_return_type);
  auto func_types = LUCOL_Debug::dbg_builder->createSubroutineType(type_list);

  // TODO: Verify that the line number info is correct - may be end of func instead of beginning
  // WARNING: Flags interface may change??? Currently set via booleans but new LLVM uses enum??
  // isLocalToUnit false, isDefinition true
  DISubprogram *subprog = LUCOL_Debug::dbg_builder->createFunction(
      dbg_context, proto->get_name(), StringRef(), unit, proto->get_loc().line,
      func_types, false, true, proto->get_loc().line,
      DINode::FlagPrototyped);

  func->setSubprogram(subprog);

  // Add the function to the top of the stack
  // TODO: Switch to std::stack
  DWARF_Info::scope_blocks.push_back(subprog);

  // Should always be zero arguments to procedures/segments
  if (!func->arg_empty())
  {
    throw LUCOL_IR_Error("Segment/procedure argument passing prohibited for subroutine \"" + proto->get_name() + "\"");
  }

  // Make sure that the statements "know" they're in function scope
  LUCOL_IR_Generator::function_stack.push(proto->get_name());

  // Actually generate the IR for the module/segment
  for (auto &statement : statements)
  {
    auto ret = statement->generate_ir();
    // nullptr returned....
    if (!ret)
    {
      // ...so clean up and return null as well
      func->eraseFromParent();
      return nullptr;
    }
  }

  // "Clean up" now that function has been generated
  LUCOL_IR_Generator::function_stack.pop();

  LUCOL_IR_Generator::ir_builder.CreateRetVoid();
  verifyFunction(*func);
  // Remove the current function from the stack
  DWARF_Info::scope_blocks.pop_back();
  return func;
}

Value *LUCOL_AST_Declaration::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());

  unsigned int num_initializers = initializer.second.size();

  // If both of these are greater than zero then something has gone very wrong -
  // array_size is only set for uninitialized arrays
  assert(std::min(num_initializers, array_size) == 0);

  // If it has a single initializer (or no initializer) and array size is 0, it can't be a vector (array)
  // For num_initializers, 1 means a single initializer, 0 means a default initializer
  bool is_scalar = (num_initializers <= 1 && array_size == 0);

  bool is_local = (!LUCOL_IR_Generator::function_stack.empty()) && (!force_global);

  Value *init_val;
  // TODO: Handle LONG VARIABLEs
  // Assume a scalar i16 by default, overwrite if we're wrong
  Type *var_type = Type::getInt16Ty(LUCOL_IR_Generator::context);

  DIType *dbg_type = DWARF_Info::get_i16_type();

  DIFile *unit = LUCOL_Debug::dbg_builder->createFile(DWARF_Info::compile_unit->getFilename(),
                                                      DWARF_Info::compile_unit->getDirectory());
  DIScope *dbg_context = (is_local) ? DWARF_Info::scope_blocks.back() : unit;

  if (is_alias)
  {
    // Now a pointer to i16
    // QUESTION: Do we have to specify address space?
    var_type = PointerType::getUnqual(var_type);
    dbg_type = DWARF_Info::get_i16_ptr_type();
  }

  if (!is_scalar)
  {
    SmallVector<Metadata *, 1> array_range;
    auto subscripts = LUCOL_Debug::dbg_builder->getOrCreateArray(array_range);
    unsigned int array_length = std::max(array_size, num_initializers);

    //Array type with size from declaration
    var_type = ArrayType::get(var_type, array_length);

    array_range.push_back(LUCOL_Debug::dbg_builder->getOrCreateSubrange(0, array_length));
    // Type is dependent on array size
    // QUESTION: Should we use a hash table to avoid repeated calls to the internal createType??
    dbg_type = LUCOL_Debug::dbg_builder->createArrayType(array_length, 16, dbg_type, subscripts);
  }

  if (!is_local)
  {
    // Apparently we don't need to do anything with the return value here
    LUCOL_Debug::dbg_builder->createGlobalVariableExpression(dbg_context, initializer.first,
                                                             StringRef(), unit, get_loc().line, dbg_type, false);
  }

  // If there's an initializer, generate the IR and verify it
  // But only if it's local, since global alias approach is not usable for locals
  // QUESTION: Is it worth avoiding the constant array initialization entirely
  // s.t. a general approach for globals and locals could be adopted??
  if (!initializer.second.empty() && (!is_local))
  {
    // Initialize an array of Constant* that will hold the generated IR
    // for each of the initializers
    std::vector<Constant *> inits(initializer.second.size());

    for (unsigned int i = 0; i < initializer.second.size(); i++)
    {
      if (is_alias)
      {
        // Prepend underscores and add numeric suffix for pseudo-variable aliases
        // that will get aggregated into array
        auto alias_name = (is_scalar ? "" : "__") + initializer.first + (is_scalar ? "" : std::to_string(i));
        // Generate the IR for the variable we're aliasing (i.e. get a pointer to it)
        auto aliasee = dyn_cast<GlobalValue>(initializer.second[i]->generate_ir());
        inits[i] = GlobalAlias::create(alias_name, aliasee);
      }

      else
      {
        inits[i] = dyn_cast<Constant>(initializer.second[i]->generate_ir());
      }
    }

    if (is_scalar)
    {
      init_val = inits[0];
    }

    else
    {
      // Stored as a Type* for generality, but casted back to ArrayType in order
      // to build the constant array
      init_val = ConstantArray::get(dyn_cast<ArrayType>(var_type), inits);
    }

    // Make sure the IR generation worked
    if (!init_val)
    {
      throw LUCOL_IR_Error("Invalid initializer for variable \"" + initializer.first + "\"");
    }
  }

  // Don't want to allocate stack space for shallow aliases
  if (is_alias && is_scalar && (!is_local))
  {
    // FIXME: Can we just throw the initializer into the symbol table?
    LUCOL_IR_Generator::symbol_table[initializer.first] = {init_val, is_alias};
    return init_val; // FIXME: Not sure how to handle arrays of aliases....
    // ...so this is almost definitely incorrect
    // FIXME: Management of aliases in the symbol table
  }

  if (is_local) // Local variables
  {
    // Create a temporary IRBuilder at the entry block for the function
    Function *func = LUCOL_IR_Generator::module->getFunction(LUCOL_IR_Generator::function_stack.top());
    IRBuilder<> temp(&func->getEntryBlock(), func->getEntryBlock().begin());

    // The actual stack allocation
    auto var = temp.CreateAlloca(var_type, nullptr, initializer.first);
    for (unsigned int i = 0; i < initializer.second.size(); i++)
    {
      std::vector<Value *> indexes;
      // Assumes no more than 2^16-sized arrays
      // Zero to index through top-level pointer
      indexes.push_back(ConstantInt::get(LUCOL_IR_Generator::context, APInt(16, 0, true)));
      if (!is_scalar)
      {
        // then actual index of array (if there is an array)
        indexes.push_back(ConstantInt::get(LUCOL_IR_Generator::context, APInt(16, i, true)));
      }
      // Now grab the pointer to the element of the array we want to modify
      auto ele = LUCOL_IR_Generator::ir_builder.CreateInBoundsGEP(var_type, var, indexes);
      // Then store that initializer element to that pointer

      auto init = initializer.second[i]->generate_ir();

      // Double check to see if the generation returned nullptr
      if (!init)
      {
        throw LUCOL_IR_Error("Invalid initializer for variable \"" + initializer.first + "\"");
      }
      LUCOL_IR_Generator::ir_builder.CreateStore(init, ele);
    }

    // Handle the debug info now that the stack allocation info is visible
    auto dbg_local = LUCOL_Debug::dbg_builder->createAutoVariable(dbg_context, initializer.first,
                                                                  unit, get_loc().line, dbg_type);

    LUCOL_Debug::dbg_builder->insertDeclare(var, dbg_local, LUCOL_Debug::dbg_builder->createExpression(),
                                            DebugLoc::get(get_loc().line, 0, dbg_context), LUCOL_IR_Generator::ir_builder.GetInsertBlock());

    // Store the local variable in the symbol table for the given function
    LUCOL_IR_Generator::local_symbols[LUCOL_IR_Generator::function_stack.top()][initializer.first] = {var, is_alias};
    return var;
  }

  else // Global variables
  {
    // Allocate the global LUCOL variable on the stack
    LUCOL_IR_Generator::module->getOrInsertGlobal(initializer.first, var_type);
    GlobalVariable *var = LUCOL_IR_Generator::module->getNamedGlobal(initializer.first);

    // If this is the DEFINE file, we have the actual global variable declaration
    if (LUCOL_IR_Generator::is_define_file)
    {
      var->setLinkage(GlobalValue::ExternalLinkage);
    }

    // Otherwise, we just "extern" the global and let it get resolved at link time
    // We know that the DEFINE file will provide a strong defition of the symbol, i.e.
    // that it is available externally
    // See the Clang src: CodeGenModule::getLLVMLinkageForDeclarator()
    else
    {
      var->setLinkage(GlobalValue::AvailableExternallyLinkage);
    }

    // QUESTION: Assuming 2-byte alignment, will this ever not be the case?
    var->setAlignment(2);

    // Mark the variable as immutable if necessary
    if (type == LUCOL_Type::CONSTANT)
    {
      var->setConstant(true);
    }

    // If there's an initializer, use it...
    if (!initializer.second.empty() && (!is_local) && array_size == 0)
    {
      // TODO: Handle initializers for stack-allocated variables
      var->setInitializer(dyn_cast<Constant>(init_val));
    }

    // QUESTION: Is it acceptable to assume zero-initialize if there's no initializer??
    else
    {
      if (array_size == 0)
      {
        // Defaulting to 16-bit signed zero
        var->setInitializer(ConstantInt::get(LUCOL_IR_Generator::context, APInt(16, 0, true)));
      }
      else
      {
        // TODO: Use ConstantAggregateZero for zero-initialized arrays
        var->setInitializer(ConstantAggregateZero::get(var_type));
      }
    }

    // Finally, add it to the symbol table and return
    LUCOL_IR_Generator::symbol_table[initializer.first] = {var, is_alias};
    return var;
  }
}

Value *LUCOL_AST_Conditional::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());

  Value *cond_var = condition->generate_ir();
  // Make sure the condition generated ok
  if (!cond_var)
  {
    return nullptr;
  }

  // "True" means != 0
  Value *cond_result = LUCOL_IR_Generator::ir_builder.CreateICmpNE(cond_var,
                                                                   ConstantInt::get(LUCOL_IR_Generator::context, APInt(16, 0, true)));

  Function *parent_func = LUCOL_IR_Generator::ir_builder.GetInsertBlock()->getParent();

  BasicBlock *if_bb = BasicBlock::Create(LUCOL_IR_Generator::context, "doif", parent_func);

  BasicBlock *else_bb = BasicBlock::Create(LUCOL_IR_Generator::context, "else");
  // End branch to satisfy requirement that LLVM blocks terminate with control flow instruction
  // so both the if/else branches will jump to this block when they end
  BasicBlock *end_cond_bb = BasicBlock::Create(LUCOL_IR_Generator::context, "end_cond");

  auto branch_block = LUCOL_IR_Generator::ir_builder.CreateCondBr(cond_result, if_bb, else_bb);

  // Backtrack and generate IR for the if branch
  LUCOL_IR_Generator::ir_builder.SetInsertPoint(if_bb);
  // Initialize to undefined in case the block is empty
  Value *if_val = UndefValue::get(Type::getVoidTy(LUCOL_IR_Generator::context));
  // Loop over each if statement
  for (auto &then_stmt : if_block)
  {
    if_val = then_stmt->generate_ir();
    if (!if_val)
    {
      return nullptr;
    }
  }
  LUCOL_IR_Generator::ir_builder.CreateBr(end_cond_bb);
  if_bb = LUCOL_IR_Generator::ir_builder.GetInsertBlock();

  // Generate IR for the else branch
  parent_func->getBasicBlockList().push_back(else_bb);
  LUCOL_IR_Generator::ir_builder.SetInsertPoint(else_bb);
  // Initialize to undefined in case the block is empty
  Value *else_val = UndefValue::get(Type::getVoidTy(LUCOL_IR_Generator::context));

  // Set the debug info for the else block
  // DWARF_Info::emit_location({get_loc().line + static_cast<unsigned int>(if_block.size()), get_loc().col});

  for (auto &else_stmt : else_block)
  {
    else_val = else_stmt->generate_ir();
    if (!else_val)
    {
      return nullptr;
    }
  }

  LUCOL_IR_Generator::ir_builder.CreateBr(end_cond_bb);
  else_bb = LUCOL_IR_Generator::ir_builder.GetInsertBlock();

  parent_func->getBasicBlockList().push_back(end_cond_bb);
  LUCOL_IR_Generator::ir_builder.SetInsertPoint(end_cond_bb);

  // No need for phi node here because all calls (and thus all blocks) return void
  return branch_block;
}

Value *LUCOL_AST_Case::generate_ir() const
{
  DWARF_Info::emit_location(get_loc());

  Value *cond_var = condition->generate_ir();
  // Make sure the condition generated ok
  if (!cond_var)
  {
    return nullptr;
  }

  Function *parent_func = LUCOL_IR_Generator::ir_builder.GetInsertBlock()->getParent();
  // The default block needs to be created before the switch
  BasicBlock *default_bb = BasicBlock::Create(LUCOL_IR_Generator::context, "default", parent_func);
  BasicBlock *end_case_bb = BasicBlock::Create(LUCOL_IR_Generator::context, "end_case");

  auto switch_instr = LUCOL_IR_Generator::ir_builder.CreateSwitch(cond_var, default_bb, case_blocks.size() + 1);

  // Generate IR for the OTHERWISE/default case
  LUCOL_IR_Generator::ir_builder.SetInsertPoint(default_bb);
  // Initialize such that empty cases are undefined
  Value *default_val = UndefValue::get(Type::getVoidTy(LUCOL_IR_Generator::context));
  for (auto &statement : default_block)
  {
    // Generate the IR and make sure it worked
    default_val = statement->generate_ir();
    if (!default_val)
    {
      return nullptr;
    }
  }

  // Mandatory branch to the end of the case statement
  LUCOL_IR_Generator::ir_builder.CreateBr(end_case_bb);
  default_bb = LUCOL_IR_Generator::ir_builder.GetInsertBlock();

  // The blocks and values for the non-default cases need to be stored for the phi node
  std::vector<BasicBlock *> case_bbs;
  std::vector<Value *> case_vals;

  for (const auto &key_value : case_blocks)
  {
    // Create the BasicBlock for the case and add the entry
    string case_name = key_value.first;
    BasicBlock *case_bb = BasicBlock::Create(LUCOL_IR_Generator::context, case_name);

    // Add the block to the function and move the "write head" to the start of it
    parent_func->getBasicBlockList().push_back(case_bb);
    LUCOL_IR_Generator::ir_builder.SetInsertPoint(case_bb);
    // Initialize to undefined in case the block is empty
    Value *case_val = UndefValue::get(Type::getVoidTy(LUCOL_IR_Generator::context));
    // Loop over each statement in the case entry
    for (auto &stmt : key_value.second)
    {
      case_val = stmt->generate_ir();
      if (!case_val)
      {
        return nullptr;
      }
    }

    LUCOL_IR_Generator::ir_builder.CreateBr(end_case_bb);
    case_bb = LUCOL_IR_Generator::ir_builder.GetInsertBlock();

    // Now that the block has been constructed, we can add it to the jump table
    for (const auto jmp_val : case_map.at(case_name))
    {
      switch_instr->addCase(ConstantInt::get(LUCOL_IR_Generator::context, APInt(16, jmp_val, true)), case_bb);
    }

    // Store the value and block so they can be used as pairs in the phi node
    case_vals.push_back(case_val);
    case_bbs.push_back(case_bb);
  }

  // Clean up by moving the "write head" to the end of the case statement
  parent_func->getBasicBlockList().push_back(end_case_bb);
  LUCOL_IR_Generator::ir_builder.SetInsertPoint(end_case_bb);

  // No need for phi node here because all calls (and thus all blocks) return void
  return switch_instr;
}
