# An LLVM frontend for LUCOL, the LUcas COntrol Language
# Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
# The full license for this code can be found in the root directory
# of this repository in a file called LICENSE.

import os
import re
import sys

proto_file = sys.argv[1]
proto_matcher = re.compile("(\w+)\s+(\w+)\s*\(\s*(.*)\)")

with open(proto_file, "r") as proto:
    prototypes = proto.read()

prototypes = re.findall(proto_matcher, prototypes)

with open("proto.out", "w") as resfile:
    for proto in prototypes:
        name = proto[1].replace("_", "").upper()
        arg_types = ["".join(arg.split()[:-1]) for arg in proto[2].split(",")]
        arg_names = []
        if arg_types:
            for i in range(len(arg_types)):
                if arg_types[i]:
                    # If the variable has a type (isn't variadic), append the name,
                    # addind a '*' if the type of that parameter is a pointer
                    arg_names.append(
                        proto[2].split(",")[i].split()[-1]
                        + ("", "*")[arg_types[i][-1] == "*"]
                    )
            variadic = "..." in proto[2]
        resfile.write(
            "{}{} {} $\n".format(("", "VARIADIC ")[variadic], name, " ".join(arg_names))
        )

# TODO: Get the segment prototypes from the GROUP file, these also need to be prototyped for the compiler

