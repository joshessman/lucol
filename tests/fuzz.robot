*** Settings ***
Documentation     LUCOL fuzzed test cases
Library           fuzz_macros.py
Test Teardown     Cleanup Files

*** Test Cases ***
Iterated Fuzz Test
    FOR    ${iteration}    IN RANGE    5
        Generate random input
        Ensure CRCs match
        Refresh condition variables
        Ensure CRCs match
        Refresh condition variables
        Ensure CRCs match
    END