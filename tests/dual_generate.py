# An LLVM frontend for LUCOL, the LUcas COntrol Language
# Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
# The full license for this code can be found in the root directory
# of this repository in a file called LICENSE.

import random
import string
from typing import Any, Dict, List, Tuple

ident_max_len = 6
max_statements = 3
max_case_blocks = 5
max_statement_depth = 4
short_max = 32767


class AST_generator(object):
    def __init__(self) -> None:
        self.global_vars: List[str] = []
        self.case_maxvals: Dict[str, int] = {}
        self.global_defs: Dict[str, int] = {}

    def generate_ident(self) -> str:
        ident_len = random.randint(1, ident_max_len)
        first_char = random.choice(string.ascii_letters)
        ident = first_char + "".join(
            random.choice(string.ascii_letters + string.digits)
            for i in range(ident_len - 1)
        )
        return ident.upper()

    def generate_statements(self, depth: int = 0) -> List[Tuple[str, str, List[str]]]:
        global global_vars
        num_statements = random.randint(1, max_statements)
        statements = []
        for i in range(num_statements):
            if depth > max_statement_depth:
                type = "call"
            else:
                type = random.choice(["call", "ifelse", "case"])
            if type == "call":
                """
                Appends tuple of form 
                ('call', 
                <func_name>, 
                [<arg1>, <arg2>,  ...])
                """
                name = "CRC"
                args = ["*", str(random.randint(0, short_max))]
                statements.append((type, name, args))
            elif type == "ifelse":
                """
                Appends tuple of form 
                ('ifelse', 
                <cond_var>, 
                [<cond_true_stmt1>,  ...], 
                [<cond_false_stmt1>, ...])
                """
                cond_var = self.generate_ident()
                # Make sure it doesn't exist already
                while cond_var in self.global_vars:
                    cond_var = self.generate_ident()
                self.global_vars.append(cond_var)
                if_cond = self.generate_statements(depth + 1)
                else_cond = self.generate_statements(depth + 1)
                statements.append((type, cond_var, if_cond, else_cond))
            elif type == "case":
                """
                Appends tuple of form 
                ('case', 
                <cond_var>, 
                [<default_stmnt1>,  ...], 
                [[<case_0_stmt1>, ...], [<case_1_stmt1>, ...], ...])
                """
                cond_var = self.generate_ident()
                # Make sure it doesn't exist already
                while cond_var in self.global_vars:
                    cond_var = self.generate_ident()
                self.global_vars.append(cond_var)
                num_conds = random.randint(1, max_case_blocks)
                self.case_maxvals[cond_var] = num_conds
                case_blocks = [
                    self.generate_statements(depth + 1) for j in range(num_conds)
                ]
                otherwise_block = self.generate_statements(depth + 1)
                statements.append((type, cond_var, otherwise_block, case_blocks))
        return statements

    def generate_segment(self) -> Tuple[str, List[Tuple[str, str, List[str]]]]:
        """
        Returns tuple of form 
        (<seg_name>, 
        [<statement1>, statement<2>, ...])
        """
        # seg_name = self.generate_ident()
        seg_name = "CRCTST"
        return (seg_name, self.generate_statements())

    def define_globals(self) -> Dict[Any, Any]:
        for var in self.global_vars:
            if var in self.case_maxvals:
                # equal chance for each of the case values
                possible_case_vals = list(range(self.case_maxvals[var]))
                # then some random value for the default case
                possible_case_vals.append(
                    random.randint(self.case_maxvals[var] + 1, short_max)
                )
                self.global_defs[var] = random.choice(possible_case_vals)
            else:
                # equal chance of zero/nonzero for even coverage of if/else
                self.global_defs[var] = random.choice([0, random.randint(1, short_max)])
        return self.global_defs


class C_generator(object):
    def __init__(
        self, AST: Tuple[str, List[Tuple[str, str, List[str]]]], globals: Dict[Any, Any]
    ) -> None:
        self.global_defs = globals
        self.program = ""
        # Define the accumulator/implicit register
        self.program += "short _LUCIF = 0;\n"
        for var in self.global_defs:
            self.program += "short {} = {};\n".format(var, self.global_defs[var])
        self.program += "\n"
        func_name = AST[0]
        self.program += "void {}(void) {{\n".format(func_name)
        func_body = AST[1]
        for statement in func_body:
            self.build_statement(statement)
        self.program += "}\n"

    def build_statement(
        self, statement: Tuple[str, str, List[str]], depth: int = 1
    ) -> None:
        st_type = statement[0]
        if st_type == "call":
            self.build_call(statement, depth + 1)
        elif st_type == "ifelse":
            self.build_ifelse(statement, depth + 1)
        elif st_type == "case":
            self.build_case(statement, depth + 1)

    def build_call(self, statement: Tuple[str, str, List[str]], depth: int = 1) -> None:
        name = statement[1]
        # Replace the * with the pointer to the accumulator
        args = ["&_LUCIF" if arg == "*" else arg for arg in statement[2]]
        self.program += ("  " * depth) + "{}({});\n".format(name, ", ".join(args))

    def build_ifelse(self, statement: Tuple[str, str, Tuple, Tuple], depth=1) -> None:
        cond_var = statement[1]
        if_block = statement[2]
        else_block = statement[3]
        self.program += ("  " * depth) + "if ({}) {{\n".format(cond_var)
        for if_st in if_block:
            self.build_statement(if_st, depth + 1)
        self.program += ("  " * depth) + "} else {\n"
        for else_st in else_block:
            self.build_statement(else_st, depth + 1)
        self.program += ("  " * depth) + "}\n"

    def build_case(self, statement: Tuple[str, str, Tuple, Tuple], depth=1) -> None:
        cond_var = statement[1]
        otherwise_block = statement[2]
        case_blocks = statement[3]
        self.program += ("  " * depth) + "switch ({}) {{\n".format(cond_var)
        for i, case in enumerate(case_blocks):
            self.program += ("  " * depth) + "case {}:\n".format(str(i))
            for case_st in case:
                self.build_statement(case_st, depth + 1)
            self.program += ("  " * depth) + "break;\n"

        self.program += ("  " * depth) + "default:\n"
        for default_st in otherwise_block:
            self.build_statement(default_st, depth + 1)
        self.program += ("  " * depth) + "}\n"


class LUCOL_generator(object):
    def __init__(
        self, AST: Tuple[str, List[Tuple[str, str, List[str]]]], globals: Dict[Any, Any]
    ) -> None:
        self.global_defs = globals
        self.program = ""
        for var in self.global_defs:
            self.program += "VARIABLE {}({})\n".format(var, self.global_defs[var])
        self.program += "\n"
        func_name = AST[0]
        self.program += "SEGMENT {}\n".format(func_name)
        func_body = AST[1]
        for statement in func_body:
            self.build_statement(statement)
        self.program += "ENDSEG\n"

    def build_statement(
        self, statement: Tuple[str, str, List[str]], depth: int = 1
    ) -> None:
        st_type = statement[0]
        if st_type == "call":
            self.build_call(statement, depth + 1)
        elif st_type == "ifelse":
            self.build_ifelse(statement, depth + 1)
        elif st_type == "case":
            self.build_case(statement, depth + 1)

    def build_call(self, statement: Tuple[str, str, List[str]], depth: int = 1) -> None:
        name = statement[1]
        args = statement[2]
        self.program += ("  " * depth) + "{}({})\n".format(name, ", ".join(args))

    def build_ifelse(self, statement: Tuple[str, str, Tuple, Tuple], depth=1) -> None:
        cond_var = statement[1]
        if_block = statement[2]
        else_block = statement[3]
        self.program += ("  " * depth) + "DOIF ({})\n".format(cond_var)
        for if_st in if_block:
            self.build_statement(if_st, depth + 1)
        self.program += ("  " * depth) + "ELSE\n"
        for else_st in else_block:
            self.build_statement(else_st, depth + 1)
        self.program += ("  " * depth) + "ENDIF\n"

    def build_case(self, statement: Tuple[str, str, Tuple, Tuple], depth=1) -> None:
        cond_var = statement[1]
        otherwise_block = statement[2]
        case_blocks = statement[3]
        case_string = "".join(
            [", CASE{}".format(str(i)) for i in range(len(case_blocks))]
        )
        self.program += ("  " * depth) + "CASE({}{})\n".format(cond_var, case_string)
        for i, case in enumerate(case_blocks):
            self.program += ("  " * depth) + "BEGIN (CASE{})\n".format(str(i))
            for case_st in case:
                self.build_statement(case_st, depth + 1)

        self.program += ("  " * depth) + "OTHERWISE\n"
        for default_st in otherwise_block:
            self.build_statement(default_st, depth + 1)
        self.program += ("  " * depth) + "ENDCASE\n"


class generator(object):
    def __init__(self) -> None:
        self._astgen = AST_generator()
        self._ast = self._astgen.generate_segment()
        globals = self._astgen.define_globals()
        self._cgen = C_generator(self._ast, globals)
        self._lucolgen = LUCOL_generator(self._ast, globals)

    def write_files(self, path_spec: str) -> None:
        with open(path_spec + ".c", "w") as cfile:
            cfile.write(self._cgen.program)

        with open(path_spec + ".luc", "w") as lucfile:
            lucfile.write(self._lucolgen.program)

    def reset_globals(self) -> None:
        globals = self._astgen.define_globals()
        self._cgen = C_generator(self._ast, globals)
        self._lucolgen = LUCOL_generator(self._ast, globals)


def main() -> None:
    astgen = AST_generator()
    ast = astgen.generate_segment()
    print("AST generated")
    globals = astgen.define_globals()
    print("Globals generated")
    cgen = C_generator(ast, globals)
    print("C generated")
    lucolgen = LUCOL_generator(ast, globals)
    print("LUCOL generated")

    with open("fuzzer_output.c", "w") as cfile:
        cfile.write(cgen.program)

    with open("fuzzer_output.luc", "w") as lucfile:
        lucfile.write(lucolgen.program)


if __name__ == "__main__":
    main()
