# An LLVM frontend for LUCOL, the LUcas COntrol Language
# Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
# The full license for this code can be found in the root directory
# of this repository in a file called LICENSE.

import os
import subprocess
from robot.api import logger


class syntax_macros(object):
    def __init__(self):
        self._LUCOL_path = os.path.join(os.pardir, "build", "LUCOL")
        self._result = ""
        self._exec_path = os.path.join(os.getcwd(), "test_exec")
        self._test_path = ""

    def compile(self, test_file_name):
        self._test_path = os.path.join(os.getcwd(), "syntax_tests", test_file_name)
        compile_result = subprocess.run(
            [
                self._LUCOL_path,
                "-p",
                os.path.join(os.pardir, "utils", "proto.out"),
                self._test_path + ".luc",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        self._result = compile_result.stderr.decode("utf-8")
        print(self._result, "is result")

    def link_test(self):
        # TODO: Windows compatibility
        # TODO: Figure out if -no-pie is actually needed or if a workaround is possible
        link_result = subprocess.run(
            [
                "gcc",
                "test_w.c",
                os.path.join(os.getcwd(), "modules", "modules.c"),
                self._test_path + ".o",
                "-o",
                self._exec_path,
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        self._result = link_result.stderr.decode("utf-8")

    def run_test(self):
        run_result = subprocess.run(
            [self._exec_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        self._result = run_result.stdout.decode("utf-8").strip()
        logger.info("Test program output: " + self._result)

    def output_should_be(self, desired_output):
        if self._result != desired_output:
            raise AssertionError("%s != %s" % (self._result, desired_output))

    def cleanup_files(self):
        test_dir = os.path.join(os.getcwd(), "syntax_tests")
        temp_files = os.listdir(test_dir)
        for file in temp_files:
            if file.endswith(".ll") or file.endswith(".o"):
                os.remove(os.path.join(test_dir, file))
        os.remove(self._exec_path)
