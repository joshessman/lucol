# An LLVM frontend for LUCOL, the LUcas COntrol Language
# Copyright (C) 2020  Josh Essman, Catherine Mittlieder, Matt Sauer
# The full license for this code can be found in the root directory
# of this repository in a file called LICENSE.

import os
import subprocess
from robot.api import logger
import shutil

import dual_generate


class fuzz_macros(object):
    def __init__(self) -> None:
        self._LUCOL_path = os.path.join(os.pardir, "build", "LUCOL")
        self.src_path = os.path.join(os.getcwd(), "fuzzer_source")
        self._mismatch = False

    def generate_random_input(self) -> None:
        self._gen = dual_generate.generator()
        self._gen.write_files(self.src_path)

    def refresh_condition_variables(self) -> None:
        self._gen.reset_globals()
        self._gen.write_files(self.src_path)

    def ensure_CRCs_match(self) -> bool:
        LUCOL_path = os.path.join(os.pardir, "build", "LUCOL")
        self._exec_path = os.path.join(os.getcwd(), "fuzz_exe")
        prog = subprocess.run(
            [
                LUCOL_path,
                "-p",
                os.path.join(os.pardir, "utils", "proto.out"),
                self.src_path + ".luc",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        logger.info(
            "LUCOL compiler stdout: {}".format(prog.stdout.decode("utf-8").strip())
        )
        logger.info(
            "LUCOL compiler stderr: {}".format(prog.stderr.decode("utf-8").strip())
        )
        prog = subprocess.run(
            [
                "gcc",
                "CRC_test_w.c",
                os.path.join(os.getcwd(), "modules", "modules.c"),
                self.src_path + ".o",
                "-o",
                self._exec_path,
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        logger.info(
            "LUCOL linking stdout: {}".format(prog.stdout.decode("utf-8").strip())
        )
        logger.info(
            "LUCOL linking stderr: {}".format(prog.stderr.decode("utf-8").strip())
        )
        luc_run = subprocess.run(
            [self._exec_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        luc_CRC = luc_run.stdout.decode("ascii").strip()
        logger.info("LUCOL test program output: {}".format(luc_CRC))
        prog = subprocess.run(
            [
                "gcc",
                "CRC_test_w.c",
                os.path.join(os.getcwd(), "modules", "modules.c"),
                self.src_path + ".c",
                "-o",
                self._exec_path,
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        c_run = subprocess.run(
            [self._exec_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        c_CRC = luc_run.stdout.decode("ascii").strip()
        logger.info("C test program output: {}".format(c_CRC))
        if luc_CRC != c_CRC:
            logger.info("Mismatch detected, saving files...")
            shutil.copyfile(
                self.src_path + ".luc",
                self.src_path + ".luc.{}.error".format(str(datetime.datetime.now())),
            )
            shutil.copyfile(
                self.src_path + ".c",
                self.src_path + ".c.{}.error".format(str(datetime.datetime.now())),
            )
            logger.error("%s != %s" % (luc_CRC, c_CRC))
            return False
        else:
            return True

    def cleanup_files(self):
        test_dir = os.getcwd()
        temp_files = os.listdir(test_dir)
        for file in temp_files:
            if (
                file.endswith(".ll")
                or file.endswith(".o")
                or file.endswith(".luc")
                or file.endswith(".c")
            ) and "test_w.c" not in file:
                os.remove(os.path.join(test_dir, file))
        os.remove(self._exec_path)

