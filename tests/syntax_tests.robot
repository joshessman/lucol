*** Settings ***
Documentation     LUCOL test cases
Test Template     Compile/Run
Library           syntax_macros.py
Test Teardown     Cleanup Files

*** Test Cases ***      Test Name   Compile_Out   Link_Out    Run_Out
Segment Test            segment     ${EMPTY}      ${EMPTY}    5
Title Test              title       ${EMPTY}      ${EMPTY}    9
Local Variable Test 1   local_var1  ${EMPTY}      ${EMPTY}    98
Local Variable Test 2   local_var2  ${EMPTY}      ${EMPTY}    92\n93\n93
Hexadecimal Test        hex         ${EMPTY}      ${EMPTY}    508
If Test 1               if1         ${EMPTY}      ${EMPTY}    19
If Test 2               if2         ${EMPTY}      ${EMPTY}    ${EMPTY}
Nested If Test          nested_if   ${EMPTY}      ${EMPTY}    327
Nested If/Else Test     nested_ifelse  ${EMPTY}   ${EMPTY}    418
Else Test               else        ${EMPTY}      ${EMPTY}    22
If/Else Test 1          ifelse1     ${EMPTY}      ${EMPTY}    19
If/Else Test 2          ifelse2     ${EMPTY}      ${EMPTY}    22
Multi-Statement Test    multi       ${EMPTY}      ${EMPTY}    47\n2027\n91\n12\n299
Assignment Test 1       assignment1  ${EMPTY}      ${EMPTY}    18\n34
Assignment Test 2       assignment2  ${EMPTY}      ${EMPTY}    18\n34
Types Test              types       ${EMPTY}      ${EMPTY}    15\n22\n0\n1
Case Test 1             case1       ${EMPTY}      ${EMPTY}    71
Case Test 2             case2       ${EMPTY}      ${EMPTY}    72
Case Test 3             case3       ${EMPTY}      ${EMPTY}    74
Case Test 4             case4       ${EMPTY}      ${EMPTY}    77
Case Test 5             case5       ${EMPTY}      ${EMPTY}    81
Case Test 6             case6       ${EMPTY}      ${EMPTY}    189
Case Test 7             case7       ${EMPTY}      ${EMPTY}    193
Case Test 8             case8       ${EMPTY}      ${EMPTY}    198
Case Test 9             case9       ${EMPTY}      ${EMPTY}    ${EMPTY}
Positive/Negative Test  pos_neg     ${EMPTY}      ${EMPTY}    32767\n-32768
Variadic Test           variadic    ${EMPTY}      ${EMPTY}    21
Defererence Test        deref       ${EMPTY}      ${EMPTY}    -1068
Array Test              array       ${EMPTY}      ${EMPTY}    0
Duplicated Case Test    case_dup1   ${EMPTY}      ${EMPTY}    191
Duplicated Case Test 2  case_dup2   ${EMPTY}      ${EMPTY}    441
Implicit Def Test       implicit    ${EMPTY}      ${EMPTY}    137
    
*** Keywords ***
Compile/Run
    [Arguments]   ${test_name}    ${compile_output}   ${link_output}   ${run_output}
    Compile   ${test_name}
    Output Should Be    ${compile_output}
    Link Test
    Output Should Be    ${link_output}
    Run Test
    Output Should Be    ${run_output}
