#include <stdio.h>
#include <stdarg.h>

void PUT(short* val)
{
  printf("%d\n", *val);
  return;
}

void PUTL(short val)
{
  printf("%d\n", val);
  return;
}

void INC(short* val)
{
  *val += 1;
  return;
}

void SUM(short* result, ...)
{
  va_list ap;
  short i;
  short total = 0;
  va_start(ap, result); 
  while (1)
  {
    short curr = (short) va_arg(ap, int);
    if (curr == 0) break; 
    total += curr;
  }
  va_end(ap);
  
  *result = total;
}

#define TABLE_SIZE 256
short CRC_table[TABLE_SIZE];

void CRC_init()
{
  // https://users.ece.cmu.edu/~koopman/crc/
  const short polynomial = 0x8fdb;
  short curr_table_val;
  
  for (short i = 0; i < TABLE_SIZE; i++)
  {
    curr_table_val = i << 8;
    
    for (char j = 8; j > 0; j--)
    {
      if (curr_table_val & 1)
      {
        curr_table_val = (curr_table_val >> 1) ^ polynomial;
      }
      
      else
      {
        curr_table_val = (curr_table_val >> 1);
      }
    }
    CRC_table[i] = curr_table_val;
  }
}

void CRC(short* context, short val)
{
  short idx;
  // Low byte
  idx = ((val>>0) & 0xff) ^ (*context >> 8);
  *context = CRC_table[idx] ^ (*context << 8);
  // High byte
  idx = ((val>>8) & 0xff) ^ (*context >> 8);
  *context = CRC_table[idx] ^ (*context << 8);
}